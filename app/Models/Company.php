<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class Company extends Model
{
    use HasFactory;

    // DOC - Attribute casting - https://laravel.com/docs/8.x/eloquent-mutators#attribute-casting
    protected $casts = [
        'is_published' => 'boolean',
    ];

    // DOC - Mass Assignment - https://laravel.com/docs/8.x/eloquent#mass-assignment-json-columns
    protected $fillable = ['name', 'siren', 'is_published', 'category', 'created_year'];

}
