require('./bootstrap');

import Alpine from 'alpinejs';

window.Alpine = Alpine;

Alpine.start();

import { createApp } from 'vue';
import CompanyIndex from './components/CompanyIndex.vue';

createApp({
    components: {
        'company-index': CompanyIndex,
    }
}).mount("#app");
