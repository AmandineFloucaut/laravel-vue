
// DOC https://v3.vuejs.org/guide/composition-api-template-refs.html#template-refs - https://v3.vuejs.org/api/refs-api.html#refs
// import { ref } from "vue";
// import axios from "axios";

// export default function companyService() {
//     const companies = ref([]);

//     const getCompanies = async () => {
//         let response = await axios.get('/api/companies');
//         companies.value = response.data.data;
//     };

//     return {
//         companies,
//         getCompanies
//     };

// }

