<?php

namespace Database\Factories;

use Illuminate\Database\Eloquent\Factories\Factory;

class CompanyFactory extends Factory
{
    /**
     * Define the model's default state.
     * //DOC - Factories - https://laravel.com/docs/8.x/database-testing#defining-model-factories
     * //DOC - Faker - https://github.com/fzaninotto/Faker
     * @return array
     */
    public function definition()
    {
        return [
            'name' => $this->faker->company(),
            'siren' => $this->faker->numerify('#########'),
            'is_published' => rand(0, 1),
            'category' => $this->faker->randomElement(['PME', 'TPE', 'Grande Entreprise']),
            'created_year' => $this->faker->year($max = 'now'),
            'created_at' => now(),
            'user_id' => $this->faker->numberBetween($min = 1, $max = 10),
        ];
    }
}
