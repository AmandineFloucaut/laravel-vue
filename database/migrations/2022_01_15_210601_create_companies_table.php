<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateCompaniesTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        /**
         * //DOC - Column types - https://laravel.com/docs/8.x/migrations#available-column-types
         * //DOC - Column modifiers - https://laravel.com/docs/8.x/migrations#column-modifiers
         */
        Schema::create('companies', function (Blueprint $table) {
            $table->id();
            $table->string('name');
            $table->integer('siren');
            $table->boolean('is_published')->nullable();
            $table->enum('category', ['PME', 'TPE', 'Grande Entreprise']);
            $table->text('activity')->nullable();
            $table->text('comment')->nullable();
            $table->string('created_year', 4);
            $table->timestamps();
            $table->foreignId('user_id')->constrained();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('companies');
    }
}
